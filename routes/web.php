<?php


Route::get('/',['as'=>'home','uses'=>'controller_pages@home']);
Route::get('presupuestos-construccion',['as'=>'presupuestos_ya','uses'=>'controller_pages@presupuestos_ya']);
Route::post('login','Auth\LoginController@login');
Route::get('logout','Auth\LoginController@logout');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class controller_pages extends Controller
{
    public function home(){
    	return view('home');
    }

    public function presupuestos_ya(){
    	return view('presupuestos_solicitados');
    }
}

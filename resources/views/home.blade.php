@extends('layout')
@section('contenido')
<section>
		<div class="block no-padding">
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-featured-sec style2">
							<ul class="main-slider-sec style2 text-arrows">
								<li class="slideHome"><img src="http://placehold.it/1600x800" alt="" /></li>
								<li class="slideHome"><img src="http://placehold.it/1600x800" alt="" /></li>
								<li class="slideHome"><img src="http://placehold.it/1600x800" alt="" /></li>
							</ul>
							<div class="job-search-sec">
								<div class="job-search style2">
									<h3>Profesionales de la construcción en tu zona</h3>
									<span>Presupuestos económicos y de calidad cerca de ti</span>
									<div class="search-job2">	
										<form>
											<div class="row no-gape">
												<div class="col-lg-7">
													<div class="job-field">
														<input type="text" placeholder="¿Que necesitas?" />
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4">
													<div class="job-field">
														<select data-placeholder="Any category" class="chosen-city">
															<option>Mechanic</option>
															<option>Web Development</option>
															<option>Car Install</option>
															<option>Shoes Slippers</option>
														</select>
													</div>
												</div>
												<div class="col-lg-2  col-md-3 col-sm-12">
													<button type="submit">BUSCAR <i class="la la-search"></i></button>
												</div>
											</div>
										</form>
									</div><!-- Job Search 2 -->
									<div class="quick-select-sec">
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="quick-select">
													<a href="#" title="">
														<i class="la la-bullhorn"></i>
														<span>Design, Art & Multimedia</span>
														<p>(22 open positions)</p>
													</a>
												</div><!-- Quick Select -->
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="quick-select">
													<a href="#" title="">
														<i class="la la-graduation-cap"></i>
														<span>Education Training</span>
														<p>(06 open positions)</p>
													</a>
												</div><!-- Quick Select -->
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="quick-select">
													<a href="#" title="">
														<i class="la la-line-chart "></i>
														<span>Accounting / Finance</span>
														<p>(03 open positions)</p>
													</a>
												</div><!-- Quick Select -->
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="quick-select">
													<a href="#" title="">
														<i class="la la-users"></i>
														<span>Human Resource</span>
														<p>(03 open positions)</p>
													</a>
												</div><!-- Quick Select -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Recent Jobs</h2>
							<span>Leading Employers already using job and talent.</span>
						</div><!-- Heading -->
						<div class="job-grid-sec">
							<div class="row">
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Web Designer / Developer</a></h3>
											<span>Massimo Artemisis</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Marketing Director</a></h3>
											<span>Massimo Artemisis</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Application Developer For Android</a></h3>
											<span>Altes Bank</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Web Designer / Developer</a></h3>
											<span>Massimo Artemisis</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Web Designer / Developer</a></h3>
											<span>MediaLab</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="job-grid">
										<div class="job-title-sec">
											<div class="c-logo"> <img src="http://placehold.it/235x115" alt="" /> </div>
											<h3><a href="#" title="">Web Designer / Developer</a></h3>
											<span>StarHealth</span>
											<span class="fav-job"><i class="la la-heart-o"></i></span>
										</div>
										<span class="job-lctn">Sacramento, California</span>
										<a  href="#" title="">APPLY NOW</a>
									</div><!-- JOB Grid -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="browse-all-cat">
							<a href="#" title="" class="style2">Load more listings</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>How It Works</h2>
							<span>Each month, more than 7 million Jobhunt turn to website in their search for work, making over <br />160,000 applications every day.
							</span>
						</div><!-- Heading -->
						<div class="how-to-sec">
							<div class="how-to">
								<span class="how-icon"><i class="la la-user"></i></span>
								<h3>Register an account</h3>
								<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-file-archive-o"></i></span>
								<h3>Specify & search your job</h3>
								<p>Browse profiles, reviews, and proposals then interview top candidates. </p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-list"></i></span>
								<h3>Apply for job</h3>
								<p>Use the Upwork platform to chat, share files, and collaborate from your desktop or on the go.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Top Company Registered</h2>
							<span>Some of the companies we've helped recruit excellent applicants over the years.</span>
						</div><!-- Heading -->
						<div class="top-company-sec">
							<div class="row" id="companies-carousel">
								<div class="col-lg-3">
									<div class="top-compnay">
										<img src="http://placehold.it/180x180" alt="" />
										<h3><a href="#" title="">Symtech</a></h3>
										<span>United States, Los Angeles</span>
										<a href="#" title="">4 Open Positon</a>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay">
										<img src="http://placehold.it/180x180" alt="" />
										<h3><a href="#" title="">Symtech</a></h3>
										<span>United States, Los Angeles</span>
										<a href="#" title="">4 Open Positon</a>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay">
										<img src="http://placehold.it/180x180" alt="" />
										<h3><a href="#" title="">Symtech</a></h3>
										<span>United States, Los Angeles</span>
										<a href="#" title="">4 Open Positon</a>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay">
										<img src="http://placehold.it/180x180" alt="" />
										<h3><a href="#" title="">Symtech</a></h3>
										<span>United States, Los Angeles</span>
										<a href="#" title="">4 Open Positon</a>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay">
										<img src="http://placehold.it/180x180" alt="" />
										<h3><a href="#" title="">Symtech</a></h3>
										<span>United States, Los Angeles</span>
										<a href="#" title="">4 Open Positon</a>
									</div><!-- Top Company -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section>
		<div class="block">
			<div data-velocity="-.1" style="background: url(http://placehold.it/1920x1000) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible layer color red"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading light">
							<h2>Projob Site Stats</h2>
							<span>Here we list our site stats and how many people we’ve helped find a job and companies have found <br />recruits. It's a pretty awesome stats area!</span>
						</div><!-- Heading -->
						<div class="stats-sec">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="stats">
										<span>18</span>
										<h5>Jobs Posted</h5>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="stats">
										<span>38</span>
										<h5>Jobs Filled</h5>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="stats">
										<span>67</span>
										<h5>Companies</h5>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="stats">
										<span>92</span>
										<h5>Members</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Kind Words From Happy Candidates</h2>
							<span>What other people thought about the service provided by JobHunt</span>
						</div><!-- Heading -->
						<div class="reviews-sec" id="reviews">
							<div class="col-lg-12">
								<div class="reviews style2">
									<img src="http://placehold.it/101x101" alt="" />
									<h3>Augusta Silva <span>Web designer</span></h3>
									<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service level that they offer!</p>
								</div><!-- Reviews -->
							</div>
							<div class="col-lg-12">
								<div class="reviews style2">
									<img src="http://placehold.it/101x101" alt="" />
									<h3>Ali Tufan <span>Web designer</span></h3>
									<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service level that they offer!</p>
								</div><!-- Reviews -->
							</div>
							<div class="col-lg-12">
								<div class="reviews style2">
									<img src="http://placehold.it/101x101" alt="" />
									<h3>Augusta Silva <span>Web designer</span></h3>
									<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service level that they offer!</p>
								</div><!-- Reviews -->
							</div>
							<div class="col-lg-12">
								<div class="reviews style2">
									<img src="http://placehold.it/101x101" alt="" />
									<h3>Ali Tufan <span>Web designer</span></h3>
									<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service level that they offer!</p>
								</div><!-- Reviews -->
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Quick Career Tips</h2>
							<span>Found by employers communicate directly with hiring managers and recruiters.</span>
						</div><!-- Heading -->
						<div class="blog-sec">
							<div class="row">
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="#" title=""><img src="http://placehold.it/360x200" alt="" /></a>
											<div class="blog-date">
												<a href="#" title="">2017 <i>March 29</i></a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="#" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="#" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="#" title=""><img src="http://placehold.it/360x200" alt="" /></a>
											<div class="blog-date">
												<a href="#" title="">2017 <i>March 29</i></a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="#" title="">11 Tips to Help You Get New Clients</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="#" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="#" title=""><img src="http://placehold.it/360x200" alt="" /></a>
											<div class="blog-date">
												<a href="#" title="">2017 <i>March 29</i></a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="#" title="">An Overworked Newspaper Editor</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="#" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Buy Our Plans And Packeges</h2>
							<span>One of our jobs has some kind of flexibility option - such as telecommuting, a part-time schedule or a flexible or flextime schedule.</span>
						</div><!-- Heading -->
						<div class="plans-sec">
							<div class="row">
								<div class="col-lg-4">
									<div class="pricetable">
										<div class="pricetable-head">
											<h3>Basic Jobs</h3>
											<h2><i>$</i>10</h2>
											<span>15 Days</span>
										</div><!-- Price Table -->
										<ul>
											<li>1 job posting</li>
											<li>0 featured job</li>
											<li>Job displayed for 20 days</li>
											<li>Premium Support 24/7</li>
										</ul>
										<a href="#" title="">BUY NOW</a>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="pricetable active">
										<div class="pricetable-head">
											<h3>Standard Jobs</h3>
											<h2><i>$</i>45</h2>
											<span>20 Days</span>
										</div><!-- Price Table -->
										<ul>
											<li>11 job posting</li>
											<li>12 featured job</li>
											<li>Job displayed for 30 days</li>
											<li>Premium Support 24/7</li>
										</ul>
										<a href="#" title="">BUY NOW</a>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="pricetable">
										<div class="pricetable-head">
											<h3>Golden Jobs</h3>
											<h2><i>$</i>80</h2>
											<span>2 Month</span>
										</div><!-- Price Table -->
										<ul>
											<li>44 job posting</li>
											<li>56 featured job</li>
											<li>Job displayed for 80 days</li>
											<li>Premium Support 24/7</li>
										</ul>
										<a href="#" title="">BUY NOW</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="subscription-sec">
							<div class="row">
								<div class="col-lg-6">
									<h3>Necesitas Ayuda ?</h3>
									<span>El equipo técnico de VipProformas estan listos las 24/7</span>
								</div>
								<div class="col-lg-6">
									<form>
										<input type="text" placeholder="Su Correo Electrónico" />
										<button type="submit"><i class="la la-paper-plane"></i></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="social-links">
							<a href="#" title="" class="fb-color"><i class="fa fa-facebook"></i> Facebook</a>
							<a href="#" title="" class="tw-color"><i class="fa fa-twitter"></i> Twitter</a>
							<a href="#" title="" class="in-color"><i class="la la-instagram"></i> Instagram</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@stop
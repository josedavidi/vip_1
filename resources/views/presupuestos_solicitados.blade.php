@extends('layout')
@section('contenido')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(http://placehold.it/1600x800) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Últimos Trabajos Solicitados</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-4 column border-right">
				 		<div class="widget">
				 			
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title open">Categorias</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p>Hola</p>
				 				</div>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title open">Ubicación</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p>Hola</p>
				 				</div>
				 			</div>
				 		</div>
				 	</aside>

				 	<div class="col-lg-8 column">
				 		<br>
				 		<div class="emply-list-sec">
				 			<div class="emply-list">
				 				<div class="emply-list-thumb">
				 					<a href="#" title=""><img src="http://placehold.it/80x80" alt="" /></a>
				 				</div>
				 				<div class="emply-list-info">
				 					<div class="emply-pstn">4 Open Position</div>
				 					<h3><a href="#" title="">King LLC</a></h3>
				 					<span>Accountancy, Human Resources</span>
				 					<h6><i class="la la-map-marker"></i> Toronto, Ontario</h6>
				 					<p>The Heavy Equipment / Grader Operator  is responsible for operating one or several types construction equipment, such as front end loader, roller, bulldozer, or excavator to move,…</p>
				 				</div>
				 			</div><!-- Employe List -->
				 			<!-- Pagination -->
				 		</div>
					</div>
				 </div>
			</div>
		</div>
	</section>

	@stop
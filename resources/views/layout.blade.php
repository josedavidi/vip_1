<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Vip Proformas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="CreativeLayers">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css" />
	<link rel="stylesheet" href="css/icons.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="css/chosen.css" />
	<link rel="stylesheet" type="text/css" href="css/colors/colors.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/vip_iso.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
	
</head>
<body>

<div class="page-loading">
	<img src="images/loader.gif" alt="" />
	<span>Cargando...</span>
</div>

<div class="theme-layout" id="scrollup">
	
	<div class="responsive-header">
		<div class="responsive-menubar">
			<div class="res-logo"><a href="{{route('home')}}" title=""><img src="images/logo-vip.png" alt="" /></a></div>
			<div class="menu-resaction">
				<div class="res-openmenu">
					<img src="images/icon.png" alt="" /> Menu
				</div>
				<div class="res-closemenu">
					<img src="images/icon2.png" alt="" /> Cerrar
				</div>
			</div>
		</div>
		<div class="responsive-opensec">
			<div class="btn-extars">
				<a href="#" title="" class="post-job-btn"><i class="fas fa-bullhorn"></i> Anunciar</a>
				<ul class="account-btns">
					<li class="signup-popup"><a title="" href="{{route('presupuestos_ya')}}"><i class="fas fa-file-alt"></i> Trabajos</a></li>
					<li class="signup-popup"><a title=""><i class="la la-key"></i> Registrarse</a></li>
					<li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Acceso Usuarios</a></li>
				</ul>
			</div><!-- Btn Extras -->
			<form class="res-search">
				<input type="text" placeholder="Job title, keywords or company name" />
				<button type="submit"><i class="la la-search"></i></button>
			</form>
			<div class="responsivemenu">
				
			</div>
		</div>
	</div>
	
	<header class="stick-top style2">
		<div class="menu-sec">
			<div class="container fluid">
				<div class="logo">
					<a href="{{route('home')}}" title=""><img src="images/logo-vip.png" alt="" /></a>
				</div><!-- Logo -->
				<div class="btn-extars">
					<a href="#" title="" class="post-job-btn"><i class="fas fa-bullhorn"></i> Anunciar</a>
					<ul class="account-btns">
						<li><a href="{{route('presupuestos_ya')}}" title=""><i class="fas fa-file-alt"></i> Trabajos</a></li>
						<li class="signup-popup"><a title=""><i class="la la-key"></i> Registrarse</a></li>
						@if(auth()->guest())
						<li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Acceso Usuarios</a></li>
						@endif
					</ul>
				</div><!-- Btn Extras -->
				<nav>
					
				</nav><!-- Menus -->
			</div>
		</div>
	</header>

	@yield('contenido')

	<footer class="gray">
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="widget">
							<div class="mega-widget">
								<div class="logo"><a href="{{route('home')}}" title=""><img src="images/logo-vip.png" alt="" /></a></div>
								<div class="links">
									<a href="#" title="">Nosotros</a>
									<a href="#" title="">Terminos & Condiciones</a>
									<a href="como-funciona.php" title="">¿Como funciona?</a>
									<a href="#" title="">Suporte</a>
									<a href="contacto.php" title="">Contactenos</a>
								</div>
								<br>
								<h3 style="font-weight:500">¿Necesitas Ayuda?</h3>
								<p>Estamos a tu disposición para aclararte cualquier duda</p>
								<br>
								<a class="llamada-popup" style="color:white;background:#10C0F2; padding:15px; text-align:center">¿TE LLAMAMOS?</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-line style2">
			<span>© VipProformas 2018 - Todos los Derechos Reservados</span>
			<a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
		</div>
	</footer>

</div>

<div class="account-popup-area signin-popup-box">
	<div class="account-popup">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Ingresar</h3>
		
		<form method="POST" action="/login">
			{!!csrf_field()!!}
			<div class="cfield">
				{{$errors->first('user','campo obligatorio')}}
				<input required type="text" name="user" placeholder="Username" value="{{old('user')}}" />
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				{{$errors->first('password','campo obligatorio')}}
				<input required type="password" name="password" placeholder="********" value="{{old('password')}}" />
				<i class="la la-key"></i>
			</div>
			<a href="#" title="">Olvidaste tu Password?</a>
			<button type="submit">Entrar</button>
		</form>

	</div>
</div><!-- LOGIN POPUP -->

<div class="account-popup-area signup-popup-box">
	<div class="account-popup">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Registrate</h3>
		<form>
			
			<div class="cfield">
				<input type="password" placeholder="********" />
				<i class="la la-key"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Email" />
				<i class="la la-envelope-o"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Celular" />
				<i class="la la-phone"></i>
			</div>
			<button type="submit">Registrate</button>
		</form>
		
	</div>
</div><!-- SIGNUP POPUP -->

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/wow.min.js" type="text/javascript"></script>
<script src="js/slick.min.js" type="text/javascript"></script>
<script src="js/parallax.js" type="text/javascript"></script>
<script src="js/select-chosen.js" type="text/javascript"></script>
<script src="js/counter.js" type="text/javascript"></script>

</body>
</html>

